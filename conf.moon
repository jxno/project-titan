love.conf = (u) ->
  u.version = "11.3"                -- The LÖVE version this game was made for (string)
  u.window.width = 800              -- The window width (number)
  u.window.height = 600             -- The window height (number)
  u.window.title = "Project Titan"  -- The window title (string)
